<?php
/**
 * @file
 * Contains \Drupal\countdown_timer\Plugin\Block\CountdownBlock
 */

namespace Drupal\countdown_timer\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Countdown timer' Block
 *
 * @Block(
 *   id = "countdown_timer_block",
 *   admin_label = @Translation("Countdown timer block"),
 *   category = @Translation("Countdown timer"),
 * )
 */
class CountdownTimerBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $eventDate = false;
    $countDownService = \Drupal::service('countdown_timer.days_left');

    $node = \Drupal::request()->attributes->get('node');
    if($node && $node->hasField("field_event_date")) {
      $eventDate = $node->get('field_event_date')->value;
    }

    return [
      '#markup' => $eventDate ? $countDownService->daysLeft(new \DateTime($eventDate)) : '',
    ];
  }

  /**
  * {@inheritdoc}
  */
  public function getCacheMaxAge() {
    return 0;
  }
}
