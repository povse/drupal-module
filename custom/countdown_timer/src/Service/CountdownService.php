<?php
/**
 * @file
 * Contains \Drupal\countdown_timer\Service\CountdownService
 */

namespace Drupal\countdown_timer\Service;

class CountdownService {

/**
 * {@inheritdoc}
 */
 public function daysLeft(\DateTime $endDate) {
    $today = new \DateTime();
    $diff = $today->diff($endDate);

    if (($endDate > $today) && ($diff->days > 0)) {
      return sprintf('Days left to event start: %d', $diff->days);
    }elseif (($endDate > $today) && ($diff->days == 0)){
      return sprintf('The event starts in %d h and %d min', $diff->h, $diff->i);
    }elseif ($diff->days == 0) {
       return "This event is happening today";
    }else {
       return "This event has ended";
    }
  }

}
