<?php
namespace Drupal\Tests\countdown_timer\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\countdown_timer\Service\countDownService;

class CountDownServiceTest extends UnitTestCase {

  /**
   * The countdown service object under test.
   *
   * @var \Drupal\countdown_timer\Service\countDownService
   */
  protected $countDownService;

  protected function setUp() {
    $this->countDownService = new CountdownService;
  }

  public function testPastEvent() {
    $pastEventDate = new \DateTime();
    $serviceResult = $this
      ->countDownService
      ->daysLeft($pastEventDate->modify("-7 days"));

    $this->assertSame("This event has ended", $serviceResult);
  }

  public function testTodayEvent() {
    $serviceResult = $this
      ->countDownService
      ->daysLeft(new \DateTime());

    $this->assertSame("This event is happening today", $serviceResult);
  }

  public function testFutureEvent() {
    $futureEventDate = new \DateTime();
    $serviceResult = $this
      ->countDownService
      ->daysLeft($futureEventDate->modify("+ 44 days"));

    $this->assertSame("Days left to event start: 44", $serviceResult);
  }

  public function testFutureEventInHours() {
    $futureEventDate = new \DateTime();
    $serviceResult = $this
      ->countDownService
      ->daysLeft($futureEventDate->modify("+ 3 hours"));

    $this->assertSame("The event starts in 2 h and 59 min", $serviceResult);
  }
}
